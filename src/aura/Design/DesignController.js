({
	performInit : function(component, event, helper) {
	  
        
                var canvas = document.querySelector(".myCanvas");

                var translatePos = {
                    x: canvas.width / 2,
                    y: canvas.height / 2
                };

                var scale = 1.0;
                var scaleMultiplier = 0.8;
                var startDragOffset = {};
                var mouseDown = false;

                // add button event listeners
                document.querySelector(".plus").addEventListener("click", function(){
                    scale /= scaleMultiplier;
                    helper.draw(scale, translatePos);
                }, false);

                document.querySelector(".minus").addEventListener("click", function(){
                    scale *= scaleMultiplier;
                    helper.draw(scale, translatePos);
                }, false);

                // add event listeners to handle screen drag
                canvas.addEventListener("mousedown", function(evt){
                    mouseDown = true;
                    startDragOffset.x = evt.clientX - translatePos.x;
                    startDragOffset.y = evt.clientY - translatePos.y;
                });

                canvas.addEventListener("mouseup", function(evt){
                    mouseDown = false;
                });

                canvas.addEventListener("mouseover", function(evt){
                    mouseDown = false;
                });

                canvas.addEventListener("mouseout", function(evt){
                    mouseDown = false;
                });

                canvas.addEventListener("mousemove", function(evt){
                    if (mouseDown) {
                        translatePos.x = evt.clientX - startDragOffset.x;
                        translatePos.y = evt.clientY - startDragOffset.y;
                        helper.draw(scale, translatePos);
                    }
                });

                helper.draw(scale, translatePos);
	}
})