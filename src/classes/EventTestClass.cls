@isTest
public class EventTestClass {
    
    @isTest
    public static void editAndUpdateEvent(){
        
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        insert camp;
        
        camp.name = 'New Event';
        camp.All_Day_Event__c = true;
        
        test.startTest();
        Eventcontroller.saveEvent(camp);
        test.stopTest();
        
        System.assertEquals('New Event', camp.name);
        System.assertEquals(true, camp.All_Day_Event__c);
        
    }
     @isTest
    public static void getCampain(){
        Campaign camp1 = new Campaign();
        camp1.name = 'oldEvent';
        camp1.All_Day_Event__c = false;
        camp1.Start_Date__c = system.now();
        camp1.End_Date__c = system.now();
        insert camp1;
        
        Campaign camp2 = new Campaign();
        camp2.name = 'NewEvent';
        camp2.All_Day_Event__c = false;
        camp2.Start_Date__c = system.now();
        camp2.End_Date__c = system.now();
        insert camp2;
        test.startTest();
        Campaign recordDetails= Eventcontroller.getcampign(camp1.Id);
        test.stopTest();
        System.assertEquals('oldEvent', recordDetails.name);

        
    }
    
    @isTest
    public static void getPickListValue(){
     List<String> options = new List<String>();
     List<String> newList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Campaign.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        test.startTest();
        newList = Eventcontroller.getPickListValue();
        test.stopTest();
        System.assertEquals(options.size(), newList.size());
        
       
    }
  
}