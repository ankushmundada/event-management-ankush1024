({
    doInit: function(component, event, helper) {

        debugger;
        var action = component.get("c.getVenueLayoutType");
        var venueLayoutId = component.get("v.eventvenueId");
        console.log("eventID" + component.get("v.eventID"));
        action.setParams({ "venueLayoutId": venueLayoutId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'ERROR') {
                console.log(a.getError());
            } else if (state === 'SUCCESS') {
                debugger;
                component.set("v.wrapperList", response.getReturnValue());
                console.log(component.get("v.wrapperList"));
                var tableList = response.getReturnValue().tables;
                var conferenceType;
                if(tableList.length != 0){
                     conferenceType = response.getReturnValue().tables[0].defaultConferenceType != null ? response.getReturnValue().tables[0].defaultConferenceType : ''; 
                }else{
                    conferenceType = '';
                }
                console.log("tableList" + tableList);
                var rowList = [];
                var columnList = [];
                var columns = response.getReturnValue().SeatsPerRow;
                var rows = response.getReturnValue().numberOfTables / columns;
                //if(rows < rows+.5 ){
                //    rows++;
                //}
                var defaultlayoutType = response.getReturnValue().defaultlayoutType;
                var totalSeats = response.getReturnValue().totalSeats;
                var SeatsPerRow = response.getReturnValue().SeatsPerRow;
                var Sections = response.getReturnValue().Sections;
                for (var i = 0; i < rows; i++) {
                    rowList.push(i);
                }
                for (var j = 0; j < columns; j++) {
                    columnList.push(j);
                }
                component.set("v.numberOfRows", rowList);
                component.set("v.numberOfColumns", columnList);
                var rowListData = [];
                var counter = 1;
                for (var row = 0; row < rows; row++) {
                    for (var column = 0; column < columns; column++) {
                        if (counter <= tableList.length) {
                            rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter - 1], "defaultlayoutType": defaultlayoutType + conferenceType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections })
                            counter++;
                        }
                    }
                }
                console.log(rowListData);
                component.set("v.tempList", rowListData);
              //  helper.doInitHelper(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    onLayoutChange: function(component, event) {
        console.log('on layout select :' + event.getSource().get('v.value')); //only works for aura:component like <ui:inputSelect/>

        var action = component.get("c.getVenueLayoutTypes");
        var layout = event.getSource().get('v.value'); //component.get("v.recordId");
        action.setParams({ layout: layout });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log("Success");
                component.set("v.layoutTypeList", result);
            }
        });
        $A.enqueueAction(action);
    },

    onTypeChange: function(component, event) {
        console.log('on type select :' + event.getSource().get('v.value')); //only works for aura:component like <ui:inputSelect/>
    },

    backToVenueDetails: function(component, event, helper) {
        debugger;
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:venuelayout",
            componentAttributes: {
                eventvenueId: component.get("v.eventvenueId")
            }
        });
        evt.fire();
        event.preventDefault();
    },
    handleApplicationEvent: function(component, event, helper) {

        debugger;
        var message = event.getParam("message");
        var sectionsBeforeEdit = event.getParam("sections");
   
        component.set("v.seatsPerTableBeforeEdit", message.NumberOfSeats);
        component.set("v.seatsPerTableAfterEdit", message.NumberOfSeats);
        component.set("v.tableNameAfterEdit", message.tableName);
        component.set("v.layoutTypeAfterEdit", message.layoutType);
        component.set("v.horizontalSeatsAfterEdit", message.HorizontalSeatsBottom);
        component.set("v.seatsPerRowForEdit", message.SeatsPerRow );
        component.set("v.sectionAfterEdit", sectionsBeforeEdit );
        
        helper.showModal(component);
    },
    onDragOver: function(component, event) {
        debugger;
        event.preventDefault();
    },

    drop: function(component, event, helper) {
        debugger;
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        if (files.length > 1) {
            return alert("You can only upload one profile picture");
        }

    },
    deleteLayout : function(component, event, helper){
    
       debugger;
       document.getElementById("modalId").style.display = "none" ;
       var action = component.get("c.saveVenueLayout");
       var eventVenueId = component.get("v.eventvenueId");
       var tablesList = component.get("v.wrapperList.tables");
       var totalSeatsAfterDelete = component.get("v.wrapperList.totalSeats") - component.get("v.seatsPerTableBeforeEdit");
       component.set("v.wrapperList.totalSeats",parseInt(totalSeatsAfterDelete));
       for(var i=0;i<tablesList.length;i++){
            if (component.get("v.tableNameAfterEdit") === tablesList[i].tableName) {
                tablesList.splice(i,1);
                component.set("v.wrapperList.numberOfTables",component.get("v.wrapperList.numberOfTables")-1);
            }
        }
        var wrapObj = component.get("v.wrapperList");
       var updatedWrapperObject = [];
       var jsonData = {};
       for (var property in wrapObj) {
       		jsonData[property] = wrapObj[property];
        }
       updatedWrapperObject.push(jsonData);
       var jsonObj = JSON.stringify(jsonData);
       console.log('jsonData'+ jsonData);
       action.setParams({ "evtVenueId": eventVenueId , "updatedJSON" : tablesList , "totalSeats" : totalSeatsAfterDelete , "updateWrapperObjectList" : jsonObj });
	    action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'ERROR') {
                console.log(a.getError());
            } else if (state === 'SUCCESS') {
                component.set("v.wrapperList", response.getReturnValue());
                console.log(component.get("v.wrapperList"));
                var tableList = response.getReturnValue().tables;
                var conferenceType;
                if(tableList.length != 0){
                     conferenceType = response.getReturnValue().tables[0].defaultConferenceType != null ? response.getReturnValue().tables[0].defaultConferenceType : ''; 
                }else{
                    conferenceType = '';
                }
                console.log("tableList" + tableList);
                var rowList = [];
                var columnList = [];
                var columns = response.getReturnValue().SeatsPerRow;
                var rows = response.getReturnValue().numberOfTables / columns;
                
                var defaultlayoutType = response.getReturnValue().defaultlayoutType;
                var totalSeats = response.getReturnValue().totalSeats;
                var SeatsPerRow = response.getReturnValue().SeatsPerRow;
                var Sections = response.getReturnValue().Sections;
                for (var i = 0; i < rows; i++) {
                    rowList.push(i);
                }
                for (var j = 0; j < columns; j++) {
                    columnList.push(j);
                }
                component.set("v.numberOfRows", rowList);
                component.set("v.numberOfColumns", columnList);
                var rowListData = [];
                var counter = 1;
                for (var row = 0; row < rows; row++) {
                    for (var column = 0; column < columns; column++) {
                        if (counter <= tableList.length) {
                            rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter - 1], "defaultlayoutType": defaultlayoutType + conferenceType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections })
                            counter++;
                        }
                    }
                }
                console.log(rowListData);
                component.set("v.tempList", rowListData);
              //  helper.doInitHelper(component, event, helper);
            }
            });
         $A.enqueueAction(action);
   },
   editLayout : function(component){
       document.getElementById("editLayoutDiv").style.display = "block" ;
   },
   saveLayout : function(component, event, helper){
    
       debugger;
     
	   document.getElementById("modalId").style.display = "none" ;
       var action = component.get("c.saveVenueLayout");
       var eventVenueId = component.get("v.eventvenueId");
       var tableList = component.get("v.wrapperList.tables");
       var upDatedSections = component.get("v.sectionAfterEdit");
       component.set("v.wrapperList.Sections",upDatedSections);
       var seatsDiff = component.get("v.seatsPerTableAfterEdit") - component.get("v.seatsPerTableBeforeEdit");
       var totalSeatsAfterEdit = parseInt(component.get("v.wrapperList.totalSeats") + seatsDiff);
       for (var i = 0; i < tableList.length; i++) {
            if (component.get("v.tableNameAfterEdit") === tableList[i].tableName) {
                tableList[i].NumberOfSeats = component.get("v.seatsPerTableAfterEdit");
                tableList[i].HorizontalSeatsBottom = component.get("v.horizontalSeatsAfterEdit");
                tableList[i].HorizontalSeatsTop = component.get("v.horizontalSeatsAfterEdit");
                tableList[i].SeatsPerRow = component.get("v.seatsPerRowForEdit");
            }
        }
       debugger;
       component.set("v.wrapperList.totalSeats",totalSeatsAfterEdit);
       var wrapObj = component.get("v.wrapperList");
       var updatedWrapperObject = [];
       var jsonData = {};
       for (var property in wrapObj) {
       		jsonData[property] = wrapObj[property];
        }
       updatedWrapperObject.push(jsonData);
       var jsonObj = JSON.stringify(jsonData);
       console.log('jsonData'+ jsonData);
       action.setParams({ "evtVenueId": eventVenueId , "updatedJSON" : tableList , "totalSeats" : totalSeatsAfterEdit , "updateWrapperObjectList" : jsonObj });
	    action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'ERROR') {
                console.log(a.getError());
            } else if (state === 'SUCCESS') {
                component.set("v.wrapperList", response.getReturnValue());
                console.log(component.get("v.wrapperList"));
                var tableList = response.getReturnValue().tables;
                var conferenceType;
                if(tableList.length != 0){
                     conferenceType = response.getReturnValue().tables[0].defaultConferenceType != null ? response.getReturnValue().tables[0].defaultConferenceType : ''; 
                }else{
                    conferenceType = '';
                }
                console.log("tableList" + tableList);
                var rowList = [];
                var columnList = [];
                var columns = response.getReturnValue().SeatsPerRow;
                var rows = response.getReturnValue().numberOfTables / columns;
                
                var defaultlayoutType = response.getReturnValue().defaultlayoutType;
                var totalSeats = response.getReturnValue().totalSeats;
                var SeatsPerRow = response.getReturnValue().SeatsPerRow;
                var Sections = response.getReturnValue().Sections;
                for (var i = 0; i < rows; i++) {
                    rowList.push(i);
                }
                for (var j = 0; j < columns; j++) {
                    columnList.push(j);
                }
                component.set("v.numberOfRows", rowList);
                component.set("v.numberOfColumns", columnList);
                var rowListData = [];
                var counter = 1;
                for (var row = 0; row < rows; row++) {
                    for (var column = 0; column < columns; column++) {
                        if (counter <= tableList.length) {
                            rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter - 1], "defaultlayoutType": defaultlayoutType + conferenceType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections })
                            counter++;
                        }
                    }
                }
                console.log(rowListData);
                component.set("v.tempList", rowListData);
               // helper.doInitHelper(component, event, helper);
            }
            });
         $A.enqueueAction(action);
   },
   cancel : function(component){
    
       document.getElementById("modalId").style.display = "none" ;
   }
})