({
	drawConference : function(component) {
	   
       debugger;
       var globalID = component.getGlobalId();
       console.log(component.getGlobalId());
      
       var obj =  component.get("v.wrapperObj");
       var parentdiv = document.getElementById(globalID); 
       parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
       var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            "message": obj
            })
        sObjectEvent.fire();
        })); 
       var totalNoOfSeats =  component.get("v.wrapperObj.NumberOfSeats");
       var horizontalSeats = component.get("v.wrapperObj.HorizontalSeatsTop");
       
       var numberOfSeatsOnLeftSide = Math.round((totalNoOfSeats - horizontalSeats*2)/2) ;
       var numberOfSeatsOnRightSide = totalNoOfSeats - numberOfSeatsOnLeftSide- horizontalSeats*2;
        
       // left Side seating arrangement start 
        
       var getLeftSeats = document.getElementById(globalID+'_leftSeating');
       var leftSideSeats = document.createElement('ul');
       leftSideSeats.id = globalID + '_leftSideUl';
       getLeftSeats.appendChild(leftSideSeats);
       var i=0; 
       for(i=0;i<numberOfSeatsOnLeftSide;i++){
           
           var leftSideSeat =  document.createElement('li');
           leftSideSeat.id = globalID + '_leftSideSeat'+i;
           leftSideSeat.className = 'chair';
           $("ul[id='"+globalID+"_leftSideUl"+"']").append(leftSideSeat);
           
		}
        var lSeats = document.getElementById(globalID+'_leftSeating');
        lSeats.style.height = 30*i+'px';
        // left Side seating arrangement end
       // Right Side seating arrangement start
	   var getRightSeats = document.getElementById(globalID+'_rightSeating');
       var rightSideSeats = document.createElement('ul');
       rightSideSeats.id = globalID + '_rightSideUl';
       getRightSeats.appendChild(rightSideSeats);
       var j=0; 
       for(j=0;j<numberOfSeatsOnRightSide;j++){
           
           var rightSideSeat =  document.createElement('li');
           rightSideSeat.id = globalID + '_rightSideSeat'+j;
           rightSideSeat.className = 'chair';
           $("ul[id='"+globalID+"_rightSideUl"+"']").append(rightSideSeat);
           
		}
        
        var lSeats = document.getElementById(globalID+'_rightSeating');
        lSeats.style.height = 30*i+'px';
        lSeats.style.marginTop = (30*i)*(-1)+ 1 +'px';
        lSeats.style.marginLeft = 50+ 10*(horizontalSeats+1) +(24*horizontalSeats)+'px';
     
      // Right Side seating arrangement end
      // Horizontal seat bottom arrangement Start
       var getHorizontalSeats = document.getElementById(globalID+'_horizontalSeatingBottom');
       var horizontalSideSeats = document.createElement('ul');
       horizontalSideSeats.id = globalID + '_horizontalBottomUl';
       horizontalSideSeats.style.position = 'relative';
       horizontalSideSeats.style.top = 4+'px';
       getHorizontalSeats.appendChild(horizontalSideSeats);
       var bottom=0;
       for(bottom=0;bottom<horizontalSeats;bottom++){
           
           var horizontalSeat =  document.createElement('li');
           horizontalSeat.id = globalID + '_horizontalBottomSeat'+bottom;
           horizontalSeat.className = 'chair';
           $("ul[id='"+globalID+"_horizontalBottomUl"+"']").append(horizontalSeat);
		}
        var hSeats = document.getElementById(globalID+'_horizontalSeatingBottom');
        hSeats.style.width = 10*(bottom+1)+24*bottom+'px';
        // Horizontal seat bottom arrangement end
       // Horizontal seat top arrangement Start
       var getHorizontalTopSeats = document.getElementById(globalID+'_horizontalSeatingTop');
       var horizontalTopSideSeats = document.createElement('ul');
       horizontalTopSideSeats.id = globalID + '_horizontalTopUl';
       horizontalTopSideSeats.className = 'horizontalSeatTop';
       horizontalTopSideSeats.style.position = 'relative';
       horizontalTopSideSeats.style.top = -27+'px'; 
       getHorizontalTopSeats.appendChild(horizontalTopSideSeats);
       var top=0;
       for(top=0;top<horizontalSeats;top++){
           
           var horizontalTopSeat =  document.createElement('li');
           horizontalTopSeat.id = globalID + '_horizontalTopSeat'+top;
           horizontalTopSeat.className = 'chair';
           $("ul[id='"+globalID+"_horizontalTopUl"+"']").append(horizontalTopSeat);
		}
        var hTopSeats = document.getElementById(globalID+'_horizontalSeatingTop');
        hTopSeats.style.width = 10*(top+1)+24*top+'px';
        hTopSeats.style.marginTop = (30*i)*(-1)+'px';
        // Horizontal seat top arrangement end
	}
})