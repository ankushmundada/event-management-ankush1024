public class DesignLayoutWrapper {
    
    @AuraEnabled
    public String defaultlayoutType  { get; set; }
    @AuraEnabled
	Public String guid { get; set; }
    @AuraEnabled
	public Integer numberOfTables { get; set; }
    @AuraEnabled
	public Table[] tables { get; set; }
    
    @AuraEnabled
	Public String ParentId { get; set; }
    
	// total seat count
	@AuraEnabled
	public Integer totalSeats  { get; set; }
    
    public Integer layoutRowCount { get; set; }
	
    // Seats per row for audience
	@AuraEnabled
    public Integer SeatsPerRow { get; set; } // change variable name -> numberOfColumns
	@AuraEnabled
    public Integer Sections { get; set; }
    // Starting sequence for a Seat, can be alphabetical
	@AuraEnabled
    public String SeatSequenceStart  { get; set; }
    
    public Miscellaneous[] miscellaneous { get; set; }
	
	// Position for the layout
    Public Position position  { get; set; }
    
	// This map will be used to map seats for a table based on registration and layout rendering purpose
	public Map<String, Event_Registration__c> seatToObjectMap;
 
    public class Table {
        @AuraEnabled
    	public String layoutType  { get; set; }
        @AuraEnabled
        public String tableName { get; set; }
        @AuraEnabled
        public String TableNumber { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String guid { get; set; }
        @AuraEnabled
        public Integer NumberOfSeats { get; set; }
        @AuraEnabled
        public Position position { get; set; }
		@AuraEnabled        
        public String height { get; set; }
        @AuraEnabled
        public String width { get; set; }
        @AuraEnabled
        public Integer SeatsPerRow { get; set; }
        @AuraEnabled
        public Integer HorizontalSeats { get; set; }
        @AuraEnabled
        public Integer HorizontalSeatsTop { get; set; }
        @AuraEnabled
        public Integer HorizontalSeatsBottom { get; set; }
        @AuraEnabled
        public String defaultConferenceType { get; set; }
      }

   
    
    public class Position {
    	public String positionX {get; set; }
    	public String positionY {get; set; }
    }

    Public class Miscellaneous {
         public String objectType{ get; set; }
           public String guid { get; set; }
          public Position position { get; set; }
         
     }

}