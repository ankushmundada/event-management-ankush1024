@isTest
public class VenueLayoutArrangementTestClass {
     @IsTest static void testEventVenues(){
          Campaign campaign = new Campaign();
        campaign.name = 'old Event';
        campaign.All_Day_Event__c = false;
        campaign.Start_Date__c = system.now();
        campaign.End_Date__c = system.now();
        campaign.Type = 'A,B,C';
        insert campaign;
           System.assertEquals(true, campaign.Id != null, ' No campaign created.' + campaign) ;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        System.assertEquals(true, venue.Id != null, ' No venue created.' + venue) ;
       
        
        Venue_Layout__c venuelayout=new Venue_Layout__c();
        venuelayout.Name = 'abc';
        venuelayout.Layout__c='custom';
        venuelayout.Type__c='Square';
        venuelayout.Venue__c=venue.Id;
               insert venuelayout;
          System.assertEquals(true, venuelayout.Id != null, ' No venuelayout created.' + venuelayout) ;
          
         Event_venue__c eventvenue = new Event_venue__c();
        eventvenue.Name = 'abc';
        eventvenue.Campaign__c = campaign.Id;
        eventvenue.Venue__c = venue.Id;
        eventvenue.Venue_Layout__c = venuelayout.Id;
        insert eventvenue;
          System.assertEquals(true, eventvenue.Id != null, ' No eventvenue created.' + eventvenue) ;
           test.startTest();
          VenueLayoutArrangementController.getEventVenues(eventvenue.Id);
           test.stoptest();
          List<Event_venue__c> newlist=[select Id,Campaign__c,Venue__c,Venue_Layout__c from Event_venue__c ];
          system.assertEquals(eventvenue.Id, newlist[0].Id);
      }

}