({
	createConference : function(component, event) {
	   
       debugger;
       var globalID = component.getGlobalId();
       console.log(component.getGlobalId());
        
       var obj =  component.get("v.wrapperObj");
       var parentdiv = document.getElementById(globalID); 
       parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
       var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            "message": obj
            })
        sObjectEvent.fire();
        }));
       var totalNoOfSeats = component.get("v.wrapperObj.NumberOfSeats");
       var horizontalSeats = component.get("v.wrapperObj.HorizontalSeatsBottom");
        //component.get("v.wrapperObj.NumberOfSeats");
       var numberOfSeatsOnLeftSide = Math.round((totalNoOfSeats - horizontalSeats)/2) ;
       var numberOfSeatsOnRightSide = totalNoOfSeats - numberOfSeatsOnLeftSide - horizontalSeats ;
        
       // left Side seating arrangement start 
        
       var getLeftSeats = document.getElementById(globalID+'_leftSeating');
       var leftSideSeats = document.createElement('ul');
       leftSideSeats.id = globalID + '_leftSideUl';
       getLeftSeats.appendChild(leftSideSeats);
       var i=0; 
       for(i=0;i<numberOfSeatsOnLeftSide;i++){
           
           var leftSideSeat =  document.createElement('li');
           leftSideSeat.id = globalID + '_leftSideSeat'+i;
           leftSideSeat.className = 'chair';
           $("ul[id='"+globalID+"_leftSideUl"+"']").append(leftSideSeat);
           
		}
        var lSeats = document.getElementById(globalID+'_leftSeating');
        lSeats.style.height = 30*i+'px';
        // left Side seating arrangement end
       // Right Side seating arrangement start
	   var getRightSeats = document.getElementById(globalID+'_rightSeating');
       var rightSideSeats = document.createElement('ul');
       rightSideSeats.id = globalID + '_rightSideUl';
       getRightSeats.appendChild(rightSideSeats);
       var j=0; 
       for(j=0;j<numberOfSeatsOnRightSide;j++){
           
           var rightSideSeat =  document.createElement('li');
           rightSideSeat.id = globalID + '_rightSideSeat'+j;
           rightSideSeat.className = 'chair';
           $("ul[id='"+globalID+"_rightSideUl"+"']").append(rightSideSeat);
           
		}
        
        var lSeats = document.getElementById(globalID+'_rightSeating');
        lSeats.style.height = 30*i+'px';
        lSeats.style.marginTop = (30*i)*(-1)+'px';
        lSeats.style.marginLeft = 90+10*(horizontalSeats+1)+(24*horizontalSeats)+'px';
     
      // Right Side seating arrangement end
      // Horizontal seat arrangement Start
       var getHorizontalSeats = document.getElementById(globalID+'_horizontalSeating');
       var horizontalSideSeats = document.createElement('ul');
       horizontalSideSeats.id = globalID + '_horizontalUl';
       getHorizontalSeats.appendChild(horizontalSideSeats);
       var k=0;
       for(k=0;k<horizontalSeats;k++){
           
           var horizontalSeat =  document.createElement('li');
           horizontalSeat.id = globalID + '_horizontalSeat'+k;
           horizontalSeat.className = 'chair';
           $("ul[id='"+globalID+"_horizontalUl"+"']").append(horizontalSeat);
		}
        var hSeats = document.getElementById(globalID+'_horizontalSeating');
        hSeats.style.width = 50+25*k+'px';
        // Horizontal seat arrangement end
	}
})