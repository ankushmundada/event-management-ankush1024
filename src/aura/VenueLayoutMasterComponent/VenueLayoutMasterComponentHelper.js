({
    doInitHelper : function(component,event,helper){
         var eventvenueId = component.get("v.eventvenueId");
       
        console.log('Init venueLayoutController');
        //component.set("v.flag",true);
    	
        var action = component.get("c.getEventVenues");
        action.setParams({ "eventvenueId" : eventvenueId});
        
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var venue = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.venue", venue);  
            }
        });
        $A.enqueueAction(action);
        
    },
    showModal : function(component) {
        
        document.getElementById("modalId").style.display = "block";
     
    }
})